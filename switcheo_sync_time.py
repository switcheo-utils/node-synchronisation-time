import requests
import json
import time
from threading import Thread
from datetime import datetime
import argparse
import random


parser = argparse.ArgumentParser()
parser.add_argument("node", nargs="?", default="http://localhost:26659", help="IP and Port of node you want to inspect")
parser.add_argument("-r", dest="reference", default=None,
                    help="IP and Port of reference node if not provided rnd public node will be selected")
parser.add_argument("--public-nodes", dest="printNodes", action="store_true", help="Print public nodes")
parser.add_argument("--time-between-requests", dest="tbr", type=float, default=5.0,
                    help="Time between status requests in seconds")

STATUS_REQUESTS = {
    "VAL": [],
    "REF": []
}
TRADESCAN = "https://tradescan.switcheo.org"
NODE = None
REFERENCE = None
TIME_BETWEEN_REQUESTS = 5.0


def get_status(ip):
    response = requests.get(ip+"/status")
    if response.status_code != 200:
        raise RuntimeError("Did not receive status code 200")
    return json.loads(response.content)


def get_validator_status():
    global NODE
    return get_status(NODE)


def get_reference_status():
    global REFERENCE
    return get_status(REFERENCE)


def get_validator_block_height():
    t = time.time()
    height = int(get_validator_status()["result"]["sync_info"]["latest_block_height"])
    STATUS_REQUESTS["VAL"].insert(0, {"time": t, "height": height})
    # Save last 10 results
    STATUS_REQUESTS["VAL"] = STATUS_REQUESTS["VAL"][:10]


def get_reference_block_height():
    t = time.time()
    height = int(get_reference_status()["result"]["sync_info"]["latest_block_height"])
    STATUS_REQUESTS["REF"].insert(0, {"time": t, "height": height})
    # Save last 10 results
    STATUS_REQUESTS["REF"] = STATUS_REQUESTS["REF"][:10]


def check_connection(ip):
    url = ip+"/status"
    try:
        response = requests.get(url, timeout=4)
        if response.status_code != 200:
            print()
            print(f"Did not receive status code 200. Got status code {response.status_code} instead.")
            print("Response Content:")
            print(response.content)
            exit(1)
    except requests.exceptions.ConnectTimeout:
        print()
        print(f"Could not get any response from {url}. Verify the IP and PORT!")
        exit(1)


def calc_validator_blocks_per_second():
    s_t = STATUS_REQUESTS["VAL"][-1]["time"]
    s_h = STATUS_REQUESTS["VAL"][-1]["height"]
    e_t = STATUS_REQUESTS["VAL"][0]["time"]
    e_h = STATUS_REQUESTS["VAL"][0]["height"]
    blocks = e_h - s_h
    duration = e_t - s_t
    return blocks / duration


def calc_reference_blocks_per_second():
    s_t = STATUS_REQUESTS["REF"][0]["time"]
    s_h = STATUS_REQUESTS["REF"][0]["height"]
    e_t = STATUS_REQUESTS["REF"][-1]["time"]
    e_h = STATUS_REQUESTS["REF"][-1]["height"]
    blocks = e_h - s_h
    duration = e_t - s_t
    return blocks / duration


def refresh_validator():
    global TIME_BETWEEN_REQUESTS
    while True:
        get_validator_block_height()
        time.sleep(TIME_BETWEEN_REQUESTS)


def refresh_reference():
    while True:
        get_reference_block_height()
        time.sleep(TIME_BETWEEN_REQUESTS)


def get_public_nodes():
    global TRADESCAN
    response = requests.get(TRADESCAN+"/monitor")
    if response.status_code != 200:
        raise RuntimeError("Did not receive status code 200")
    return json.loads(response.content)


def print_public_nodes():
    public_nodes = get_public_nodes()
    for i, node in enumerate(public_nodes):
        print(f"{i+1:2}: {node['ip']+':26657':22} - {node['country']} - {node['moniker']}")


def main():
    global NODE, REFERENCE, TIME_BETWEEN_REQUESTS
    args, unknown = parser.parse_known_args()

    if args.printNodes:
        print("Fetch public nodes...")
        print_public_nodes()
        exit(0)

    if args.tbr < 1.0:
        print("Time between requests has to be min 1.0 seconds!")
        exit(1)

    TIME_BETWEEN_REQUESTS = args.tbr

    NODE = args.node

    if not NODE.startswith("http"):
        NODE = "http://"+NODE

    print(f"Check connection to node: {NODE}...", end=" ")
    check_connection(NODE)
    print("Connection is fine.")

    REFERENCE = args.reference
    if not REFERENCE:
        print("No reference node provided. Fetch public nodes... ")
        public_nodes = get_public_nodes()
        count = len(public_nodes)
        select = random.randint(0, count)
        selected_node = public_nodes[select]
        ip = "http://" + selected_node['ip'] + ":26657"
        print(f"Found {count} public node(s). Selected: {selected_node['moniker']}({ip} - {selected_node['country']})")
        REFERENCE = ip

    if not REFERENCE.startswith("http"):
        REFERENCE = "http://"+REFERENCE

    print(f"Check connection to node: {REFERENCE}...", end=" ")
    check_connection(REFERENCE)
    print("Connection is fine.")

    print("Calculate synchronisation speed for node, wait few seconds ... ", end="", flush=True)

    Thread(target=refresh_validator).start()
    Thread(target=refresh_reference).start()

    while len(STATUS_REQUESTS["VAL"]) < 2 and len(STATUS_REQUESTS["REF"]) < 2:
        time.sleep(1)

    print("Done.")

    while True:
        v_bps = calc_validator_blocks_per_second()
        v_height = STATUS_REQUESTS["VAL"][0]["height"]
        n_bps = calc_reference_blocks_per_second()
        n_height = STATUS_REQUESTS["REF"][0]["height"]
        x = (n_height - v_height) / ((v_bps - n_bps) or 1.0)
        ts = datetime.utcfromtimestamp(time.time() + x).strftime('%Y-%m-%d %H:%M:%S')
        print("\r", end="")
        print(f"Estimated synchronisation time(local): {ts} ({v_height}/{n_height}). ", end="", flush=True)
        print(f"Node blocks per second: {v_bps:.2f}. ", end="", flush=True)
        print(f"Reference blocks per second: {n_bps:.2f}.", end="", flush=True)
        time.sleep(1)


if __name__ == '__main__':
    main()
