# **Tradehub Node Synchronization Time**

This small Python program is designed to provide node operators with an estimated synchronization time. The synchronization speed of the own node is calculated. To make the program work a reference node is needed. From this reference node the block speed is calculated. 
<br>
<br>
![img](./img/example.png)
<br>
## Math
For those who are interested, the following is the (not quite correct) mathematical approach.
The calculation looks like this:<br>
![equation](./img/eq0.png)

![equation](./img/eq1.png)

By equating the two equations, the seconds to the point of intersection (seconds until synchronization time) can be determined:

![equation](./img/eq2.png)

## Requirements
For execution **Python3.6 or higher is needed**, because the new f-strings are used for formatting. No additional Python modules are needed. Only the standard modules: 
- requests
- json
- time
- threading
- datetime
- argparse
- random

## Installation
Just download it. ;) The program does not have to be located on the node host, it can be run on a completely different computer. 

## Usage
Go to the directory where the file is located and start the py file with the correct Python installation and pass the arguments.

### Compare local node with public random remote node
This call compares the local node with a random public node.<br>
```python3 switcheo_sync_time.py localhost:26657```

### Compare remote node with public random remote node
This call compares the node 18.141.90.114(switcheo-mainnet-sentry-01-node) with a random public node.<br>
```python3 switcheo_sync_time.py 18.141.90.114:26657```

### Compare remote node with specific remote node
This call compares the node 18.141.90.114(switcheo-mainnet-sentry-01-node) with a specific remote node 46.137.251.194(switcheo-mainnet-sentry-02-node). Reference node does not need to be public.<br>
```python3 switcheo_sync_time.py 18.141.90.114:26657 -r 46.137.251.194:26657```

### List Public Nodes
To list all public nodes you can call the program like this:
<br>
```python3 switcheo_sync_time.py --public-nodes```
<br>
![img](./img/public_nodes.png)

## Support
This project was developed and published by Devel484 To support other projects and to improve the infrastructure of Tradehub you can stake or donate your STWH to the Validator [```Devel[oper]```](https://switcheo.org/validator/swthvaloper1vwges9p847l9csj8ehrlgzajhmt4fcq4dmg8x0?net=main).

## Disclaimer
This program was developed and published by community member Devel484. Switcheo is not in direct contact with this program. Neither Switcheo nor Devel484 are liable for any losses.
